<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \Illuminate\Contracts\Support\Renderable;


// TODO this is for admin panel, we dont need this yet.
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Renders the main page.
     *
     * @return Renderable
     */
    public function index() : Renderable
    {
        return view('pages.home');
    }
}
