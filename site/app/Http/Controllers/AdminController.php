<?php

namespace App\Http\Controllers;


use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Contracts\View\View;


class AdminController extends Controller
{
    private $user;
    public function __construct()
    {
        $this->middleware('auth');
        $this->user = Auth::user();
    }


    /**
     * Renders admin panel menu
     *
     *
     * @return View
     */
    public function index(): View
    {
        return view('admin/admin', ['user' => $this->user]);
    }
}
