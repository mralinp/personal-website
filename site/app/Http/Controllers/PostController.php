<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use \Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;


class PostController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request): Response
    {
        return response()->json(["message" => "store"], 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  Post  $post
     * @return Renderable
     */
    public function show(Post $post)
    {
        if(!$post)
        {
            abort(404, "This content does not exist.");
        }
        return view('pages/post', ['post' => $post]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Post  $post
     * @return Response
     */
    public function update(Request $request, Post $post)
    {
        return response()->json(["message" => "update"], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Post $post
     * @return Response
     */
    public function destroy(Post $post)
    {
        return response()->json(["message" => "destroy"], 200);
    }
}
