<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset("/bootstrap.css")}}">
    <link rel="stylesheet" href="{{asset("css/custom-rtl.css")}}">
    <!-- Prism -->
    <link rel="stylesheet" href="{{asset("css/prism.css")}}"/>
    <!-- OR -->
    <link rel="stylesheet" href="{{asset("css/bootstrap-rtl.min.css")}}">

    {{-- Title --}}
    <title>
        @yield('title')
    </title>
    {{-- End Title --}}
</head>
    <body>
        {{-- Nav Bar --}}
        <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark" style="text-align: right">
            <a class="navbar-brand" href="/">علی نادری پاریزی</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="/">خانه <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/blog">بلاگ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/projects">پروژه‌ها</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/resume">رزومه</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/about">درباره من</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0" action="/search" method="post">
                    @csrf
                    <input name="input" class="form-control mr-sm-2" type="text" style="text-align: right" dir="rtl" placeholder="به دنبال چیزی هستید؟" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">جستجو</button>
                </form>
            </div>
        </nav>
        {{-- End Nav Bar --}}

        {{-- Main content --}}
        @yield('content')
        {{-- End Main content --}}
        <hr>
        {{-- Footer --}}
        <footer class="footer text-center">
            <div class="container text-center">
                <div class="container">
                    <!-- Example row of columns -->
                    <div class="row">
                        <div class="col-md-4">
                            <h2>تجربیات</h2>
                            <p>زندگی پراز فراز و نشیب‌هاست، ‌تجربیات به آدمها کمک میکنن تا پیشرفت کنن
                                و استفاده از تجربیات دیگران سرعت این پیشرفت رو افزایش میده.
                                من هم به عنوان موجودی دوپا! تجربیاتی داشته و خواهم داشت.
                            </p>
                            <p>در این قسمت سعی میکنم خاطره بنویسم.</p>
                            <p><a class="btn btn-secondary" href="#" role="button">تجربیات &raquo;</a></p>
                        </div>
                        <div class="col-md-4">
                            <h2>یادگیری ماشین</h2>
                            <p>یادگیری ماشین شاخه مورد علاقه من در دوره ارشده. سعی میکنم مطالبی رو که یادگرفتم و چیزهایی که تجربه کردم رو به اشتراک بذارم تا هم خودم یادم بمونه و شاید هم بدرد فرد دیگری در این جهان خاکی بخوره.</p>
                            <p><a class="btn btn-secondary" href="#" role="button">یادگیری ماشین &raquo;</a></p>
                        </div>

                        <div class="col-md-4">
                            <h2>معرفی کتاب</h2>
                            <p>معمولا ماهی یکی دوتا کتاب میخونم،‌ توی این قسمت سعی میکنم خلاصه‌ای از کتاب‌هایی که خوندم رو بذارم.</p>
                            <p><a class="btn btn-secondary" href="#" role="button">کتاب خوانی &raquo;</a></p>
                        </div>
                    </div>
                </div>
            </div>

            <hr>
            <span class="text-muted">استفاده از تمام مطالب این سایت آزاد است.</span>
        </footer>
        {{-- End footer --}}
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>
        <script src="{{asset("js/bootstrap.min.js")}}"></script>
        <!-- Prism -->
        <script src="{{asset("js/prism.js")}}"></script>
    </body>
</html>
