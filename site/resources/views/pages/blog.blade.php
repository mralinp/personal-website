@extends('layouts.main')

@section('title')
    بلاگ
@endsection

@section('content')

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
        <div class="container" >
            <h1 class="display-3"  dir="rtl">بلاگ</h1>
            <p>
                سعی میکنم هر دفعه در مورد یه موضوع مطلبی رو بنویسم.
            </p>
            <p>
                موضوعات مورد علاقه من یادگیری ماشین، بینایی کامپیوتر و رباتیک، مهندسی معکوس، وب و سیستمهای نهفته هستند.
            </p>
        </div>
    </div>

    <div class="container">
        <div class="row">

            {{--Blog post--}}
            <div class="col-md-6">
                <div class="card flex-md-row mb-4 box-shadow h-md-250">
                    <div class="card-body d-flex flex-column align-items-start">
                        <strong class="d-inline-block mb-2 text-primary">علوم کامپیوتر</strong>
                        <h3 class="mb-0">
                            <a class="text-dark" href="#">تقطیر دانش</a>
                        </h3>
                        <div class="mb-1 text-muted">۱۲ اردیبهشت ۱۴۰۰</div>
                        <p class="card-text mb-auto">
                            امروزه با پیشرفت شبکه‌های عصبی
                            و سخت‌افزار‌های قدرتمند، استفاده از این شبکه ها در حل مسائل روزمره افزایش یافته است.
                            این شبکه‌ها میتوانند مسائل پیچیده‌ای را با دقت خوبی حل کنند اما آیا آنها برای پیاده سازی
                            بر روی دستگاه‌های کوچک مناسب‌اند؟
                        </p>
                        <p><a class="btn btn-secondary" href="#" role="button">ادامه مطلب &raquo;</a></p>

                    </div>
                    <img
                        style="margin:20px"
                        class="card-img-right flex-auto d-none d-md-block"
                        width="200px"
                        height="350px"
                        src="images/kd.jpg"
                        alt="Card image cap"
                    >
                </div>
            </div>
            {{--End of blog post--}}
        </div>
    </div>

@endsection
