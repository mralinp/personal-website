@extends('layouts.main')

@section('title')
    پروژه‌های
@endsection

@section('content')

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
        <div class="container" >
            <h1 class="display-3"  dir="rtl"> پروژه‌ها</h1>
            <p>
                پروژه هایی که در طول این مدت انجام دادم رو اینجا به اشتراک میذارم،
                میتونید کدهای مرجع رو در گیتهاب من پیدا کنید. اینجا پروژه هارو با جزئیات توضیح میدم.
            </p>
        </div>
    </div>

    <div class="container">
        <div class="row">

            {{--Blog post--}}
            @foreach ($posts as $post)
            <div class="col-md-6">
                <div class="card flex-md-row mb-4 box-shadow h-md-250">
                    <div class="card-body d-flex flex-column align-items-start">
                        <strong class="d-inline-block mb-2 text-primary">علوم کامپیوتر</strong>
                        <h3 class="mb-0">
                            <a class="text-dark" href="#">{{ $post->title }}</a>
                        </h3>
                        <div class="mb-1 text-muted"> {{ $post->createdAt }} </div>
                        <p class="card-text mb-auto">
                            {{ $post->abstract }}
                        </p>
                        <p><a class="btn btn-secondary" href="/post/{{ $post->id }}}" role="button">ادامه مطلب &raquo;</a></p>

                    </div>
                    <img
                        style="margin:20px"
                        class="card-img-right flex-auto d-none d-md-block"
                        width="200px"
                        height="350px"
                        src="{{ $post->titleImage }}"
                        alt="Card image cap"
                    >
                </div>
            </div>
            @endforeach
            {{--End of blog post--}}

        </div>
    </div>

@endsection
