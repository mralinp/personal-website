@extends('layouts/main')

@section('title')
    علی پاریزی
@endsection

 @section('content')
    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
        <div class="container" >
            <h1 class="display-3"  dir="rtl">سلام دنیا!</h1>
            <p>
                من علی نادری پاریزی، کشاورز زاده‌ای اهل استان کرمان هستم.
                من عاشق کامپیوتر هستم و لیسانس خودم رو توی همین رشته (مهندسی کامپیوتر) و از دانشگاه شیراز گرفتم.
                در حال حاضر هم دانشجوی ارشد رشته هوش مصنوعی در دانشگاه علم و صنعت ایران هستم.
            </p>
            <p><a class="btn btn-primary btn-lg" href="/about" role="button"> درباره من &raquo;</a></p>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-2">
                <h3>مطالب اخیر</h3>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="card flex-md-row mb-4 box-shadow h-md-250">
                    <div class="card-body d-flex flex-column align-items-start">
                        <strong class="d-inline-block mb-2 text-primary">علوم کامپیوتر</strong>
                        <h3 class="mb-0">
                            <a class="text-dark" href="#">تقطیر دانش</a>
                        </h3>
                        <div class="mb-1 text-muted">۱۲ اردیبهشت ۱۴۰۰</div>
                        <p class="card-text mb-auto">
                            امروزه با پیشرفت شبکه‌های عصبی
                            و سخت‌افزار‌های قدرتمند، استفاده از این شبکه ها در حل مسائل روزمره افزایش یافته است.
                            این شبکه‌ها میتوانند مسائل پیچیده‌ای را با دقت خوبی حل کنند اما آیا آنها برای پیاده سازی
                            بر روی دستگاه‌های کوچک مناسب‌اند؟
                        </p>
                        <p><a class="btn btn-secondary" href="#" role="button">ادامه مطلب &raquo;</a></p>

                    </div>
                    <img
                        style="margin:20px"
                        class="card-img-right flex-auto d-none d-md-block"
                        width="200px"
                        height="350px"
                        src="images/kd.jpg"
                        alt="Card image cap"
                    >
                </div>
            </div>

            <div class="col-md-6">
                <div class="card flex-md-row mb-4 box-shadow h-md-250">
                    <div class="card-body d-flex flex-column align-items-start">
                        <strong class="d-inline-block mb-2 text-primary">کتاب</strong>
                        <h3 class="mb-0">
                            <a class="text-dark" href="#">کار عمیق</a>
                        </h3>
                        <div class="mb-1 text-muted">۱۲ بهمن ۱۳۹۹</div>
                        <p class="card-text mb-auto">
                            کار عمیق یک ابر قدرته، یک نیرو محرکه خوب که هرکسی نداره. این قدرت به ادما توانایی خستگی ناپذیر بودن رو میده و باعث میشه آثار بینقص خلق کنن...
                        </p>
                        <p><a class="btn btn-secondary" href="#" role="button">ادامه مطلب &raquo;</a></p>

                    </div>
                    <img
                        style="margin:20px"
                        class="card-img-right flex-auto d-none d-md-block"
                        width="200px"
                        height="350px"
                        src="https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Laravel.svg/1200px-Laravel.svg.png"
                        alt="Card image cap"
                    >
                </div>
            </div>

            <div class="col-md-6">
                <div class="card flex-md-row mb-4 box-shadow h-md-250">
                    <div class="card-body d-flex flex-column align-items-start">
                        <strong class="d-inline-block mb-2 text-primary">علوم کامپیوتر</strong>
                        <h3 class="mb-0">
                            <a class="text-dark" href="#">تقطیر دانش</a>
                        </h3>
                        <div class="mb-1 text-muted">۱۲ اردیبهشت ۱۴۰۰</div>
                        <p class="card-text mb-auto">
                            امروزه با پیشرفت شبکه‌های عصبی
                            و سخت‌افزار‌های قدرتمند، استفاده از این شبکه ها در حل مسائل روزمره افزایش یافته است.
                            این شبکه‌ها میتوانند مسائل پیچیده‌ای را با دقت خوبی حل کنند اما آیا آنها برای پیاده سازی
                            بر روی دستگاه‌های کوچک مناسب‌اند؟
                        </p>
                        <p><a class="btn btn-secondary" href="#" role="button">ادامه مطلب &raquo;</a></p>

                    </div>
                    <img
                        style="margin:20px"
                        class="card-img-right flex-auto d-none d-md-block"
                        width="200px"
                        height="350px"
                        src="images/kd.jpg"
                        alt="Card image cap"
                    >
                </div>
            </div>

            <div class="col-md-6">
                <div class="card flex-md-row mb-4 box-shadow h-md-250">
                    <div class="card-body d-flex flex-column align-items-start">
                        <strong class="d-inline-block mb-2 text-primary">علوم کامپیوتر</strong>
                        <h3 class="mb-0">
                            <a class="text-dark" href="#">تقطیر دانش</a>
                        </h3>
                        <div class="mb-1 text-muted">۱۲ اردیبهشت ۱۴۰۰</div>
                        <p class="card-text mb-auto">
                            امروزه با پیشرفت شبکه‌های عصبی
                            و سخت‌افزار‌های قدرتمند، استفاده از این شبکه ها در حل مسائل روزمره افزایش یافته است.
                            این شبکه‌ها میتوانند مسائل پیچیده‌ای را با دقت خوبی حل کنند اما آیا آنها برای پیاده سازی
                            بر روی دستگاه‌های کوچک مناسب‌اند؟
                        </p>
                        <p><a class="btn btn-secondary" href="#" role="button">ادامه مطلب &raquo;</a></p>

                    </div>
                    <img
                        style="margin:20px"
                        class="card-img-right flex-auto d-none d-md-block"
                        width="200px"
                        height="350px"
                        src="images/kd.jpg"
                        alt="Card image cap"
                    >
                </div>
            </div>

        </div>
    </div>

 @endsection
