@extends('layouts/main')

@section('title')
    post
@endsection

@section('content')

    <div class="jumbotron text-center">
        <div class="container">
            {{--title--}}
            <h3 class="mt-5"> {{ $post->title }}</h3>
            {{--end title--}}
        </div>
    </div>

    <div class="container text-center">
        {{--title img--}}
        <img class="rounded" width="100%" style="max-height: 400px" src="{{asset($post['title-img'])}}"/>
        {{--end title img--}}
    </div>

    <div class="container mt-5">
        <div class="blog-post">
            {{--title--}}
            <h2 class="blog-post-title">{{$post->title}}</h2>
            {{--end title--}}
            <p class="blog-post-meta"> نوشته شده در: {{$post['created_at']}} </p>

            {{-- Abstract --}}
            <h3> خلاصه مطلب </h3>
            <p>
            {{$post->abstract}}
            </p>
            {{--End Abstract--}}

            {{$post->content}}

        </div>
        <!-- end blog-post -->
    </div>
@endsection
