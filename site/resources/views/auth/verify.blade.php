@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('صحت پست الکترونیک وارد شده را تایید کنید') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('یک ایمیل تایید به پست الکترونیک شما ارسال شد.') }}
                        </div>
                    @endif

                    {{ __('لطفا پست الکترونیک خود را نگاه کنید و بر روی لینک تایید ارسال شده کلیک کنید.') }}
                    {{ __('چیزی دریافت نکردید؟') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('ارسال مجدد') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
