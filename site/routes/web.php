<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::prefix('admin')->middleware(['auth'])->group(function () {
    Route::get('', [AdminController::class, 'index']);
});

Route::get('', [HomeController::class, 'index'] );

Route::get('about', function (){
    return View('pages/about');
});

Route::resource('post', PostController::class);

Route::post('search', function(){
    return response()->json(['search' => request()->all()], 200);
});

Route::get('blog', function(){
    return View('pages/blog');
});

Route::get('projects', function(){
    return View('pages/projects');
});

Route::get('resume', function() {
    return response()->download(storage_path('app/public/cv/cv.pdf'));
});

Auth::routes(['register' => false]);


Route::get('home', [HomeController::class, 'index'])->name('home');
Route::get('lv-admin', [HomeController::class, 'admin']);
