//Navbar.js
import React from 'react';
import './navbar.css';
import Logo from '../logo.svg';
const Navbar=() => {
  return (
    <header>

        <div className="logo">
            {/* your logo */}
        </div>
        <nav className="navigation">
            {/* your navigation */}
        </nav>

    </header>
  )
};
export default Navbar;