<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="300"></a></p>

# Inrto
My personal website source code that could be used for anyone who wants to have a small personal website or a shared blog with some friends. This is a fullstack web application which it's backend is written by **Laravel** ,a really cool **PHP** freamwork, and the fronend is written on **React-JS**. React, also is a simple, powerfully and lovely **JavaScript** freamework for frontend web application development. 

Each parts could be extended and used separetly. The backend part which is a rest API is capable to be used for many kinds of subjects like blog and froume websites. You can review the API documentation which is a postman collectin and is located in the `postman` directory on the project.
# How it works
## Baseics
This is a Laravel rest API plus a React web application ui which will worke separatly by cooperatin toghether.

To run the codes, you need these requarments:

* php 7.2.4 (or above)
* composer
* Laravel 8.4
* NodeJs
* npm
* React-JS

If you want to use this as your own website, you don't need to change any thing on backend API (you can review and customize as you wish but also, you can leave it there too). 

The only thing that you need is a template, if you dont like mine, change the ui as you mind, and go threw.
# Installation
## Backend
```shell
    ~$ This is a shell script for backend.
```
## Frontend
```shell
    ~$ this is a shell script for frontend
```
## Deployment
> As it's my personal experance and still under development some features and descused abilities will be usable just after first release.